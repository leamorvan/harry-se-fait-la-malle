caisse = [200, 100, 100, 100, 50, 20, 10, 2, 2, 2, 2, 2]
rendu = [0, 8, 62, 231, 497, 842]



def rendu_monnaie2 (somme_a_rendre, caisse):
   """
    Calcule la monnaie à rendre à l'acheteur avec 
    le moins de pièces/billets possible
    
    Entrée : billets disponibles dans la caisse (liste d'entiers)
            et somme à rendre (entier)
    Sortie : monnaie rendue, avec valeur et nombre de billets (dictionnaire)
    """
   
   print(f"Nous devons vous rendre {somme_a_rendre} euros")
   monnaie_rendue = {200 : 0, 100 : 0,  50 : 0, 20 : 0, 10 : 0, 2 : 0}
   if sum(caisse) < somme_a_rendre:   #on vérifie s'il y a assez d'argent en caisse
       print("Le rendu de monnaie sera incomplet")
   for i in range(len(caisse)):
       if somme_a_rendre >= caisse[i]:          
            monnaie_rendue[caisse[i]] += 1 
            somme_a_rendre -= caisse[i]
   if somme_a_rendre != 0:
       print(f"Nous vous devons encore {somme_a_rendre} euros")
      
      
   return monnaie_rendue


for element in rendu:
   print(rendu_monnaie2(element, caisse))