#coding : 'utf-8'

'''
Mini-projet "Harry se fait la malle"

Liste de fournitures scolaires

Auteur : Titouan Plu , Léa Morvan, Caroline Chandelier

Date de création : 01/22
'''
#CONSTANTES

POIDS_MAXIMAL = 4

#variables

fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

#Fonctions globales

def tri_insertion(tab_fournitures, categorie):
    for i in range(len(tab_fournitures)):
        while (tab_fournitures[i][categorie] > tab_fournitures[i - 1][categorie]) and (i > 0) :
            tab_fournitures[i], tab_fournitures[i - 1]  = tab_fournitures[i - 1], tab_fournitures[i]
            i = i - 1
    return tab_fournitures


#Fonction pour remplissage sans critères
    
def remplissage_malle_vrac(tab_fournitures, poids_max) :
    """
    remplit la malle sans méthode particulière jusqu'à ce que la malle
    atteigne son poids maximal (4)

    Entrée : liste fournitures (table de dictionnaires), poids maximal (entier)
    Sortie : dictionnaire

    """
    malle_vrac = {}
    for objet in tab_fournitures:
        if objet['Poids'] <= poids_max:   
            malle_vrac[objet['Nom']] = objet['Poids']
            poids_max -= objet['Poids']
    return malle_vrac


def calcul_poids(rubrique):
    """
    calcule le poids de la malle avec tous les objets qu'il
    a pu mettre dedans sans depasser le poids maximal

    Entrée :malle remplie(liste de dictionnaires)
    Sortie: poids de la malle (entier)

    """
    poids = sum(rubrique.values())
    return poids


#Fonction pour maximiser le poids
    
def remplissage_malle_lourde(fournitures_triees, poids_max):
    """
    remplit la malle le plus lourdement possible
    sans dépasser le poids maximal (4)

    Entrée : fournitures_scolaires (table de dictionnaires), poids maximal (entier)
    Sortie : dictionnaire

    """
    malle_lourde = {}
    for objet in fournitures_triees:
        if objet['Poids'] <= poids_max:   #test pour savoir si on a dépassé le Poids max
            malle_lourde[objet['Nom']] = objet['Poids']
            poids_max -= objet['Poids']
    return malle_lourde



#Fonction pour le plus de mana

def remplissage_malle_mana(fournitures_triees, poids_max, critere) :  
    """
    remplit la malle avec le plus de mana possible
    sans dépasser le poids maximal (4)

    Entrée : fournitures_scolaires (table de dictionnaires), poids maximal (entier)
    Sortie : dictionnaire  

    """
    malle_mana = {}
    for objet in fournitures_triees:
        if objet['Poids'] <= poids_max:   
            malle_mana[objet['Nom']] = objet[critere]
            poids_max -= objet['Poids']
    return malle_mana



#IHM
choix = int(input("Bonjour, Harry a un problème. \
Aidez le à choisir comment il doit ranger sa malle:\n \n 1) Peu importe, pas de critères : \
Tapez 1\n 2) En maximisant la masse de la malle : Tapez 2 \n 3) En maximisant la mana : Tapez 3\n \nAllez-y :"))
while choix == 1 or choix == 2 or choix == 3:
    print(f"\nVous avez choisi le choix n°{choix}")
#Remplir la malle...n'importe comment (partie a )
    if choix == 1 :
        malle_remplie = remplissage_malle_vrac(fournitures_scolaires, POIDS_MAXIMAL)
        poids_final = calcul_poids(malle_remplie)
        print(f"Harry a pu mettre dans sa malle ces objets :\n")

        def affichage_cle(malle_finale, poids_fini) :
            for element in malle_remplie.keys():
                print(element)

        affichage_cle(malle_remplie, poids_final)
        print(f'\nSa malle pèse donc {poids_final}\n')
#Remplir la malle le plus lourdement possible (partie b)
    elif choix == 2 :
        fournitures_triees = tri_insertion(fournitures_scolaires, 'Poids')
        malle_remplie_lourde = remplissage_malle_lourde(fournitures_scolaires, POIDS_MAXIMAL)
        poids_malle_lourde = calcul_poids(malle_remplie_lourde)
        print("\nPour que la malle soit la plus lourde, Harry peut mettre dans\
              sa malle ces objets :\n ")

        def affichage_cle(malle_finie, poids_max) :
            for element in malle_remplie_lourde.keys():
                print(element)

        affichage_final = affichage_cle(poids_malle_lourde, poids_malle_lourde)
        print(f"\nSa malle pèsera donc {poids_malle_lourde}\n")
#Remplir la malle avec le maximum de mana (partie c)
    else :
        fournitures_triees = tri_insertion(fournitures_scolaires, 'Mana')
        mana_remplie = remplissage_malle_mana(fournitures_scolaires, POIDS_MAXIMAL, 'Mana')
        mana_remplie_b = remplissage_malle_mana(fournitures_scolaires, POIDS_MAXIMAL, 'Poids')
        poids_malle_mana = calcul_poids(mana_remplie_b)
        quantité_mana_total = calcul_poids(mana_remplie)
        print("\nPour que la malle ai le plus de mana sans dépasser un poids\
              de 4 Harry peut metrre dans sa malle ces objets:\n ")

        def affichage_cle(malle_finie, mana_max) :
            for element in mana_remplie.keys():
                print(element)

        affichage_final = affichage_cle(mana_remplie, quantité_mana_total)
        print(f"\nSa malle aura donc {quantité_mana_total} de mana pour un poids de {poids_malle_mana} \n")

    choix = int(input("Si vous voulez arrêter de ranger sa malle tapez 0\
                      \nMais si vous voulez changer la façon de ranger sa malle :\
                       1) En maximisant la masse de la malle : Tapez 2\
                        2) En maximisant la mana : Tapez 3\n \nAllez-y :"))

    if choix == 0 :
        print("Merci de votre aide !")

if choix != 1 and choix != 2 and choix != 3 and choix != 0 :
    print("Désolé je n'ai pas compris votre demande, veuillez-réessayer en\
          cliquant sur 'ctrl + Entrée'")
