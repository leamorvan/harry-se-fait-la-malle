"""
VARIABLES
    caisse : dictionnaire d'entiers
    somme_a_rendre : entier
    monnaie_rendue : dictionnaire contenant le billet 
                 utilisé (en clé) et le nombre de billets utilisés (en valeur)
    rendu : liste des sommes à rendre automatiquement 
    i : entier itérable 
    element : entier itérable 
    longueur_caisse : entier
DEBUT
    monnaie_rendue ← {200 : 0, 100 : 0,  50 : 0, 20 : 0, 10 : 0, 2 : 0}
    SI la somme valeurs caisse < somme à rendre ALORS
        ECRIRE ("Le rendu de monnaie sera incomplet")
    FIN_SI
    POUR i ALLANT_DE 0 A (longueur_caisse - 1)
        SI somme_a_rendre >= caisse[i] ALORS 
            monnaie_rendue[caisse[i]] + 1
            somme_a_rendre - caisse[i]
        FIN_SI
    FIN_POUR
    SI somme_a_rendre != 0 
        ECRIRE("Nous vous devons encore {somme_a_rendre}")
    FIN_SI    
    RETOURNE monnaie_rendue
FIN
"""